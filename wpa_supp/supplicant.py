#!/usr/bin/python
"""
It is get dbus get, set, getall signal information
"""

import dbus
from dbus.mainloop.glib import DBusGMainLoop
import gobject


class Supplicant(object):

    """
    Get supplicant properties information's
    """

    def __init__(self):
        DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SystemBus()
        self.properties = None
        self.wpas_dbus_name = 'fi.w1.wpa_supplicant1'
        self.wpas_dbus_interface = None
        self.wpas_dbus_opath = None
        self.proxy = None
        self.iface = None

    def get_proxy(self):
        """
        It is interact with a remote object
        :return: dbus.proxies.ProxyObject
        """
        try:
            self.proxy = self.bus.get_object(
                self.wpas_dbus_name, self.wpas_dbus_opath)
        except dbus.DBusException as ex:
            print ex
        return self.proxy

    def get_iface(self):
        """
        get interface information
        :return: dbus interface it is a string,
        It is slash-separated
        """
        self.iface = dbus.Interface(self.proxy, self.wpas_dbus_interface)
        return self.iface

    def get_prop(self, prop_name):
        """
        Get native APIs properties or attributes information's
        :param prop_name: native API properties name
        :return: dbus properties
        """
        if self.wpas_dbus_interface is not None:
            self.properties = self.proxy.Get(
                self.wpas_dbus_interface,
                prop_name,
                dbus_interface=dbus.PROPERTIES_IFACE)
            return self.properties
        print "Check interface"

    def making_method_calls(self):
        """
        It is get busname, object path
        """
        self.get_proxy()
        self.get_iface()

    def scan_signal(self, handler_function, signal_name=None,
                    dbus_interface=None, bus_name=None,
                    path=None, **keywords):
        """
        :param handler_function: function name
        :param signal_name: properties signal
        :param dbus_interface: dbus interface name
        :param bus_name: dbus bus name
        :param path: This object path
        :param keywords: it is optional
        """
        self.bus.add_signal_receiver(
            handler_function, signal_name, dbus_interface,
            bus_name, path, **keywords)
        self._loop = gobject.MainLoop()
        self._loop.run()
