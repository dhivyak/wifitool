#!/usr/bin/python

"""
Get user input and valid ssid and dbus path informations are available
"""


class Aplist(object):

    """
    List the valid ap information of wifi list
    """

    def __init__(self, ap_list_path):
        """
        It is used available ap infromation
        """
        self._ap_list_path = ap_list_path
        self._serial_no = 1
        self._ssid_dict = dict()
        for ssid in self._ap_list_path.keys():
            self._ssid_dict[self._serial_no] = ssid
            self._serial_no += 1

    def listed(self):
        """
        It is used print the ssid with serial no
        :return: dict of ssidno
        """
        for serial_no, ssid in self._ssid_dict.iteritems():
            print "%d  %s " % (serial_no, ssid)
        return self._ssid_dict

    def valid_ssid(self, ssid_info):
        """
        First check  ssid name after that serial number.
        It is return ssid with serial number
        It is also printed
        :param ssid_info: It is usr input
        :return: return ssid name
        """
        if ssid_info in self._ssid_dict.values():
            return ssid_info

        try:
            ssid_info = eval(ssid_info)
        except NameError as error:
            print str(error)

        if ssid_info in self._ssid_dict.keys():
            return self._ssid_dict[ssid_info]
        else:
            print "Enter valid ssid or number"

    def path(self):
        """
        It return the ssid path of dbus
        :return: ssid path
        """
        ssid_info = raw_input("Enter AP list number or AP Name > ")
        ssid_name = self.valid_ssid(ssid_info)
        if ssid_name:
            return self._ap_list_path[ssid_name]
