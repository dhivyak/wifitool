#!/usr/bin/python
"""
It is object related to network interface added
"""
from supplicant import Supplicant
from supplicant1 import Supplicant1
import dbus


class Iface(Supplicant):

    """
    Interface instance to scan, get properties and so on.
    """

    def __init__(self, ifname):
        """
        It is used interface related activities like create, disconnect,
        Scanning
        :param ifname: interface name ex wlan0,
        """
        super(Iface, self).__init__()
        supp1 = Supplicant1()
        self.wpas_dbus_opath = supp1.get_interface(ifname)
        self.wpas_dbus_interface = "fi.w1.wpa_supplicant1.Interface"
        self.bsss = None

    def scan(self):
        """
        Triggers a scan
        """
        # print self.iface.bus_name
        # print self.iface.dbus_interface
        # print self.iface.object_path
        try:
            self.iface.Scan({'Type': 'active'})
            self.scan_signal(self.scan_done, signal_name="ScanDone",
                             dbus_interface=self.wpas_dbus_interface)
        except dbus.exceptions.DBusException as ex:
            print ex

    def scan_done(self, success):
        """
        It is scan done return the bss properties informations
        :param success: serial no of success
        """
        print "Scan Done: success=%s" % success
        self.bsss = self.get_prop("BSSs")
        self._loop.quit()

# if __name__ == "__main__":
#     inter = Iface("wlan0")
#     inter.making_method_calls()
#     inter.scan()
#     print inter.bsss
