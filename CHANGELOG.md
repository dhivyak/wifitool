# Change Log
## [0.1.0] - 2017-05-08

### Added
- Logic for getting interface as user input.

### Changed
- Reformat bssid print statement
- Remove unwanted print statement

## [0.0.1] - 2017-04-19

### Added
- added bss properties

### Changed
- move to code in classes

