"""
Packages wpa supplicant
"""
from setuptools import setup, find_packages
from wpa_supp import version

setup(
    name="wifitool",
    version=version.__VERSION__,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'wifitool = wpa_supp.__main__:main'
        ]
    },
)
